<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> <?php echo get_versi(); ?>
    </div>
    <strong>&copy; 2013 - <?php echo date("Y") ?> <a target="_blank" href="http://sekolahsaya.net"> sekolahsaya.net</a></strong>
 </footer>